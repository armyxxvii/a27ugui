﻿using UnityEngine;

public class UIElementTip : UIElementTipBase
{
    [SerializeField] protected TipSet[] tipSets;

    private void Awake()
    {
        if (!tips.Contains(this))
            RefreshAllIcons();
    }

    private void OnDestroy()
    {
        tips.Remove(this);
    }

    protected override void SwitchIcons()
    {
        //foreach (var tg in tipContainers)
        //{
        //    for (int i = 0; i < tg.icons.Length; i++)
        //    {
        //        tg.icons[i].icon.sprite = IsUseMouse ? tg.icons[i].mouseSprite : tg.icons[i].padSprite;
        //    }
        //}
        for (int i = 0; i < tipSets.Length; i++)
        {
            TipSet ts = tipSets[i];
            if (ts.mouseIcon != null) ts.mouseIcon.gameObject.SetActive(IsUseMouse);
            else Debug.LogErrorFormat("{0}.tipSets[{1}].mouseIcon is Null", name, i);

            if (ts.padIcon != null) ts.padIcon.gameObject.SetActive(!IsUseMouse);
            else Debug.LogErrorFormat("{0}.tipSets[{1}].padIcon is null", name, i);
        }
    }
    
    public override void RefreshIcons(bool a_callFromBase)
    {
        if (!a_callFromBase) RefreshDic();

        foreach (TipSet ts in tipSets)
        {
            string command = ts.type.ToString();
            if (ts.mouseIcon != null)
            {
                bool found = false;
                if (kb.ContainsKey(command))
                {
                    string kbdKey = kb[command];
                    if (kbdDic.ContainsKey(kbdKey))
                    {
                        found = true;
                        ts.mouseIcon.sprite = kbdDic[kbdKey];
                        ts.mouseIcon.SetNativeSize();
                    }
                }
                if (!found && mb.ContainsKey(command))
                {
                    string mseKey = mb[command].ToString();
                    if (mseDic.ContainsKey(mseKey))
                    {
                        found = true;
                        ts.mouseIcon.sprite = mseDic[mseKey];
                        ts.mouseIcon.SetNativeSize();
                    }
                }
                if (!found)
                {
                    ts.mouseIcon.sprite = mseDic["unknown"];
                    ts.mouseIcon.SetNativeSize();
                    Debug.LogErrorFormat(this, " keyBinding doesn't contains command : {0}", command);
                }
            }

            if (ts.padIcon != null)
            {
                if (pb.ContainsKey(command))
                {
                    string padKey = pb[command].ToString();
                    ts.padIcon.sprite = padDic[padDic.ContainsKey(padKey) ? padKey : "unknown"];
                    ts.padIcon.SetNativeSize();
                }
                else
                    Debug.LogErrorFormat(this, " padBinding doesn't contains command : {0}", command);
            }
        }
    }
}
