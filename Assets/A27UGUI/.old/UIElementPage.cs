﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class UIElementPage : MonoBehaviour
{
    public RectTransform listRoot;
    public GameObject tab;
    public List<UIElementBrand> list = new List<UIElementBrand>();
    public int m_rowCount = 10;
    public int m_columnCount = 1;

    [NonSerialized] public IPageOwner owner;
    [NonSerialized] public UIElementBrand selected;
    [NonSerialized] public int m_index = -1;

    [SerializeField] CanvasGroup canvasGroup;
    [SerializeField] GameObject BrandStylePrefab;
    [SerializeField] Text m_currentPage;
    [SerializeField] Text m_totalPage;
    [SerializeField] float animTime = 0.1f;

    int currentPage = -1;
    public int CurrentPage
    {
        get { return currentPage; }
        set
        {
            if (m_currentPage != null)
                m_currentPage.text = value.ToString();
            if (currentPage == value) return;
            currentPage = value;
            StopCoroutine(PageSwitchAnimation());
            if (gameObject.activeInHierarchy)
                StartCoroutine(PageSwitchAnimation());
            else
            {
                Vector2 wrkV = listRoot.anchoredPosition;
                wrkV.x = listRoot.GetComponent<GridLayoutGroup>().cellSize.x * (1 - currentPage) * m_columnCount;
                listRoot.anchoredPosition = wrkV;
            }
        }
    }

    public int TotalPage
    {
        get { return Mathf.CeilToInt((float)list.Count / (m_rowCount * m_columnCount)); }
        set
        {
            if (m_totalPage != null)
                m_totalPage.text = value.ToString();
        }
    }

    public void Init(IPageOwner a_owner)
    {
        UIElementBrand[] objs = GetComponentsInChildren<UIElementBrand>();
        for (int i = 0; i < objs.Length; i++)
        {
            Destroy(objs[i]);
        }
        list.Clear();
        owner = a_owner;
        CurrentPage = 1;
        TotalPage = 1;
        animTime = Mathf.Max(0.005f, animTime);
    }

    public void SetActive(bool a_active)
    {
        listRoot.gameObject.SetActive(a_active);
        if (tab != null)
            tab.SetActive(a_active);
    }

    public void SwitchPage(bool a_isPlus)
    {
        if (list.Count < 1) return;
        int movement = m_rowCount * m_columnCount * (a_isPlus ? 1 : -1);
        int newIndex = Mathf.Clamp(m_index + movement, 0, list.Count - 1);
        selected = list[newIndex];
        StartCoroutine(CanvasLayerControl.Select(selected.GetComponent<A27Button>()));
    }

    public void BrandSelected(UIElementBrand a_brand)
    {
        selected = a_brand;
        m_index = list.FindIndex(x => x.Equals(a_brand));
        CurrentPage = Mathf.CeilToInt((m_index + 0.5f) / (m_rowCount * m_columnCount));
        TotalPage = Mathf.CeilToInt((float)list.Count / (m_rowCount * m_columnCount));
        owner.BrandSelected(a_brand);
    }

    public void BrandClicked(UIElementBrand a_brand)
    {
        owner.BrandClicked(a_brand);
    }

    System.Collections.IEnumerator PageSwitchAnimation()
    {
        canvasGroup.blocksRaycasts = false;
        yield return null;

        float oldX = listRoot.anchoredPosition.x;
        float newX = listRoot.GetComponent<GridLayoutGroup>().cellSize.x * (1 - currentPage) * m_columnCount;
        Vector2 wrkV = listRoot.anchoredPosition;
        float time = animTime;
        while (time > 0)
        {
            wrkV.x = Mathf.Lerp(newX, oldX, time / animTime);
            listRoot.anchoredPosition = wrkV;
            time -= Time.deltaTime;
            yield return null;
        }
        wrkV.x = newX;
        listRoot.anchoredPosition = wrkV;
        yield return null;

        canvasGroup.blocksRaycasts = true;
        listRoot.SendMessage("UpdatePageInfo", SendMessageOptions.DontRequireReceiver);
        yield break;
    }

    public void UpdateData(ref List<BrandData> a_dataList)
    {
        while (list.Count != a_dataList.Count)
        {
            if (list.Count > a_dataList.Count)
            {
                Destroy(list[list.Count - 1].gameObject);
                list.RemoveAt(list.Count - 1);
            }
            else
            {
                var newB = Instantiate(BrandStylePrefab, listRoot);
                list.Add(newB.GetComponent<UIElementBrand>());
            }
        }
        for (int i = 0; i < list.Count; i++)
        {
            list[i].transform.SetAsLastSibling();
            list[i].name = "Brand[" + i.ToString() + "]:" + a_dataList[i].id.ToString();
            list[i].Set(a_dataList[i], this);
        }
        if (list.Count > 0 && selected == null) selected = list[0];
    }
}
