﻿using UnityEngine;
using UnityEngine.Events;

public class A27UIToolbar : MonoBehaviour
{
    public A27UIToolbarButton[] @Buttons;
    public UnityEvent SelectChange;

    // Use this for initialization
    void Start()
    {
        //mButtons = gameObject.GetComponentsInChildren<ButtonBase>();

        for (int i = 0; i < @Buttons.Length; ++i)
        {
            //@Buttons[i].Index = i;
            //@Buttons[i].ButtonGroup = this;
            @Buttons[i].ShowSelect(i == 0);
        }
    }

    private int _CurrentSelect;
    public int CurrentSelect
    {
        get
        {
            return _CurrentSelect;
        }

        set
        {
            _CurrentSelect = value;

            for (int i = 0; i < @Buttons.Length; ++i)
            {
                @Buttons[i].ShowSelect(i == value);
            }

            SelectChange.Invoke();
        }
    }

    public void SelectPreviousButton()
    {
        CurrentSelect--;
        if (CurrentSelect < 0)
        {
            CurrentSelect = @Buttons.Length - 1;
        }
    }

    public void SelectNextButton()
    {
        CurrentSelect++;
        if (CurrentSelect >= @Buttons.Length)
        {
            CurrentSelect = 0;
        }
    }

    public void OnPointerClick()
    {
        @Buttons[CurrentSelect].OnPointerClick(null);
    }
}
