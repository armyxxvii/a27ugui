﻿using UnityEngine;
using UnityEngine.UI;

public class UIElementControl : MonoBehaviour
{
    public string Command
    {
        get
        {
            return m_cmdType.ToString();
        }
    }

    public DeviceType m_type;
    public CrossInputType m_cmdType;
    public Image m_icon;
    public Image m_seperation;
    public Image[] m_extraIcons;
    public A27Button m_button;

    public void Set(Sprite a_icon, bool a_visible = true, bool a_error = false)
    {
        if (m_button != null)
            m_button.targetGraphic.color = a_error ? Color.red : Color.white;

        m_icon.sprite = a_icon;
        m_icon.SetNativeSize();
        foreach (Image img in m_extraIcons)
        {
            img.sprite = a_icon;
            img.SetNativeSize();
        }
        if (m_seperation != null)
            m_seperation.gameObject.SetActive(a_visible);
    }
}
