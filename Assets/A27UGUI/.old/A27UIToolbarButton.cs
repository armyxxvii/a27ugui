﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[System.Serializable]
public class IntEvent : UnityEvent<int>
{
}


[AddComponentMenu("UI/A27-ToolbarButton", 10)]
public class A27UIToolbarButton : Selectable,
    IPointerEnterHandler,
    IPointerExitHandler,
    IPointerClickHandler
{
    public Text @Text;
    public Image @Image;
    public UnityEvent Click;
    public IntEvent ClickGrid;

    public int Index;
    public A27UIToolbar ButtonGroup;

    protected override void Awake()
    {
        transition = Transition.None;
        navigation = new Navigation();
    }

    public virtual void ShowSelect(bool aShow)
    {
        if (@Image != null)
        {
            @Image.enabled = aShow;
        }
    }

    public override void OnPointerEnter(PointerEventData eventData)
    {
        if (ButtonGroup != null)
        {
            ButtonGroup.CurrentSelect = Index;
        }
    }

    public override void OnPointerExit(PointerEventData eventData)
    {
    }

    public virtual void OnPointerClick(PointerEventData eventData)
    {
        if (eventData != null)
        {
            if (eventData.button != PointerEventData.InputButton.Left)
            {
                return;
            }
        }

        if (ButtonGroup != null)
        {
            ButtonGroup.CurrentSelect = Index;
        }

        Click.Invoke();
        ClickGrid.Invoke(Index);
    }
}