﻿using UnityEngine;
using UnityEngine.UI;

public class UIElementBar : MonoBehaviour
{
    [SerializeField] protected Text m_nameText;
    [SerializeField] protected int m_nameId = 0;

    [SerializeField, Space] protected SaveType m_MaxType;
    [SerializeField] protected RectTransform m_sizeControl;
    [SerializeField] protected float m_maxValue = 100;
    [SerializeField] protected bool m_lock;
    [SerializeField] protected int m_minWidth = 100;
    [SerializeField] protected int m_maxWidth = 500;
    [SerializeField] protected float m_scale = 1.0f;
    [SerializeField] protected float m_mainScale = 1.0f;

    [SerializeField, Space] protected SaveType m_statusType;
    [SerializeField] protected Image m_statusImage;
    [SerializeField] protected float m_statusValue;

    [SerializeField, Space] protected Image m_costImage;
    [SerializeField] protected Text m_damageText;
    [SerializeField] protected float m_costValue = 100.0f;
    [SerializeField] protected float m_holdSecs = 0.5f;
    [SerializeField] protected float m_restHoldSecs;
    [SerializeField, Range(0.01f, 10f)] protected float m_plusSpeed = 0.1f;
    [SerializeField, Range(-10f, -0.01f)] protected float m_minusSpeed = -0.1f;

    [SerializeField, Space] protected RawImage ruler;
    [SerializeField] protected float m_rulerSize = 100;

    void Update()
    {
        if (m_restHoldSecs > 0)
        {
            m_restHoldSecs -= Time.deltaTime;
        }
        else
        {
            bool doCost = Mathf.Abs(m_costValue - m_statusValue) > 1;
            if (doCost)
            {
                bool isPlus = m_statusValue > m_costValue;
                m_costValue += Time.deltaTime * m_maxValue * m_scale / (isPlus ? m_plusSpeed : m_minusSpeed);

                if (isPlus != (m_statusValue > m_costValue))//數值超過
                    m_costValue = m_statusValue;
            }
            else
            {
                m_costValue = m_statusValue;
            }

            RefreshCost(doCost);
        }
    }

    public void Translate()
    {
        if (m_nameText != null)
            m_nameText.text = LocaleLoader.GetString("CharName" + m_nameId.ToString());
    }

    protected void SizeChanged()
    {
        if (!m_lock) m_maxValue = UIPoolHelper.GetSaveType(m_MaxType);
        m_scale = Mathf.Clamp(m_maxValue, m_minWidth, m_maxWidth) / m_maxValue;
        if (m_sizeControl != null) SetWidth(m_sizeControl, m_maxValue);

        if (ruler != null)
        {
            Rect newRect = ruler.uvRect;
            newRect.width = m_maxValue / m_rulerSize;
            ruler.uvRect = newRect;
        }

        m_costValue = m_statusValue;
        if (m_statusImage != null) SetWidth(m_statusImage.rectTransform, m_statusValue);
        if (m_costImage != null) SetWidth(m_costImage.rectTransform, m_costValue);
    }

    protected void ValueChanged()
    {
        m_statusValue = UIPoolHelper.GetSaveType(m_statusType);
        m_restHoldSecs = m_holdSecs;
        if (m_statusImage != null) SetWidth(m_statusImage.rectTransform, m_statusValue);

        int diff = Mathf.CeilToInt(Mathf.Abs(m_costValue - m_statusValue));
        if (m_damageText != null) m_damageText.text = diff.ToString();

        RefreshCost(diff > 0);
    }

    protected void RefreshCost(bool a_showDamage)
    {
        if (m_costImage != null) SetWidth(m_costImage.rectTransform, m_costValue);
        if (m_damageText != null) m_damageText.enabled = a_showDamage;
    }

    protected void SetWidth(RectTransform a_transform, float a_width)
    {
        if (a_transform != null)
        {
            Vector2 newSize = a_transform.sizeDelta;
            newSize.x = a_width * m_scale * m_mainScale;
            a_transform.sizeDelta = newSize;
        }
    }
}
