﻿/// <summary>
/// For BgmController.ButtonSfx use...
/// </summary>
public enum ButtonActType
{
    Select,
    Submit,
    Cancel,
    Achieve,
    Flip,

    count,
    none = -1,
}
