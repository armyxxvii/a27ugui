﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIElementPC : UIElementBar
{
    /*
    [Header("PC Settings")]
    [SerializeField] Image m_headIcon;
    [SerializeField] Text m_name;
    [SerializeField] Text m_lv;
    [SerializeField] PrefabInPrefab effect;

    [SerializeField,Space] UIElementPanel m_statusPanel;
    [SerializeField] float m_statusWaiting = 5f;
    [SerializeField] Text[] m_statusTexts = new Text[5];

    int id = -1;
    int lv = -1;

    void Update()
    {
        if (m_restHoldSecs > 0)
        {
            m_restHoldSecs -= Time.deltaTime;
        }
        else
        {
            float diff = 0;

            if (m_statusValue >= m_costValue)
            {
                m_costValue += Time.deltaTime * m_maxValue * m_scale / m_plusSpeed;
                if (m_statusValue < m_costValue) m_costValue = m_statusValue;
                diff = Mathf.CeilToInt(Mathf.Abs(m_statusValue - m_costValue));
            }
            else
            {
                m_costValue += Time.deltaTime * alwaysPlusTemp * m_scale / m_plusSpeed;
                diff = Mathf.CeilToInt(Mathf.Abs(m_statusValue + alwaysPlusTemp - m_costValue));

                if (m_costValue < alwaysPlusTemp)
                {
                    if (m_statusImage != null) SetWidth(m_statusImage.rectTransform, alwaysPlusTemp);
                }
                else//超過上限
                {
                    m_costValue -= alwaysPlusTemp;

                    int currentLv = SummonerStorage.Instance.CurrentContent.ExpLevel;
                    if (lv != currentLv)
                    {
                        m_lv.text = "LV. " + currentLv.ToString();

                        if (!m_lock) m_maxValue = UIPoolHelper.GetSaveType(m_MaxType);
                        m_scale = Mathf.Clamp(m_maxValue, m_minWidth, m_maxWidth) / m_maxValue;

                        if (m_sizeControl != null) SetWidth(m_sizeControl, m_maxValue);

                        if (m_statusImage != null) SetWidth(m_statusImage.rectTransform, m_statusValue);
                        BgmController.ButtonSfx(ButtonActType.Achieve);
                        effect.StartCoroutine(PlayEffect(lv, currentLv));
                        lv = currentLv;
                    }
                }
            }

            if (m_damageText != null)
            {
                m_damageText.text = diff.ToString();
                m_damageText.enabled = diff > 0;
            }

            if (m_costImage != null)
            {
                SetWidth(m_costImage.rectTransform, m_costValue);
            }
        }
    }

    IEnumerator PlayEffect(int a_oldLv,int a_newLv)
    {
        effect.enabled = false;
        yield return null;
        effect.enabled = true;
        Panel.Canvas.SendMessage("HealthChanged", SendMessageOptions.DontRequireReceiver);
        Panel.Canvas.SendMessage("StaminaChanged", SendMessageOptions.DontRequireReceiver);
        //更新升級屬性資料
        {
            int cId = AvatarGroupHelper.GetAvatarBase(SummonerStorage.Instance.LastSummoner);
            int diff_HP = AvatarGroupHelper.GetValue(cId, SealType.HP, a_newLv);
            diff_HP -= AvatarGroupHelper.GetValue(cId, SealType.HP, a_oldLv);
            m_statusTexts[0].text = "+ " + diff_HP.ToString();
            m_statusTexts[0].gameObject.SetActive(diff_HP > 0);

            int diff_ST = AvatarGroupHelper.GetValue(cId, SealType.ST, a_newLv);
            diff_ST -= AvatarGroupHelper.GetValue(cId, SealType.ST, a_oldLv);
            m_statusTexts[1].text = "+ " + diff_ST.ToString();
            m_statusTexts[1].gameObject.SetActive(diff_ST > 0);

            int diff_ATK = AvatarGroupHelper.GetValue(cId, SealType.ATK, a_newLv);
            diff_ATK -= AvatarGroupHelper.GetValue(cId, SealType.ATK, a_oldLv);
            m_statusTexts[2].text = "+ " + diff_ATK.ToString();
            m_statusTexts[2].gameObject.SetActive(diff_ATK > 0);

            int diff_DEF = AvatarGroupHelper.GetValue(cId, SealType.DEF, a_newLv);
            diff_DEF -= AvatarGroupHelper.GetValue(cId, SealType.DEF, a_oldLv);
            m_statusTexts[3].text = "+ " + diff_DEF.ToString();
            m_statusTexts[3].gameObject.SetActive(diff_DEF > 0);

            int diff_RES = AvatarGroupHelper.GetValue(cId, SealType.RES, a_newLv);
            diff_RES -= AvatarGroupHelper.GetValue(cId, SealType.RES, a_oldLv);
            m_statusTexts[4].text = "+ " + diff_RES.ToString();
            m_statusTexts[4].gameObject.SetActive(diff_RES > 0);
        }
        yield return new WaitForSeconds(m_statusPanel.Switch(1) + m_statusWaiting);
        m_statusPanel.Switch(0);
    }

    int alwaysPlusTemp = -1;
    new void SizeChanged()
    {
        m_statusValue = UIPoolHelper.GetSaveType(m_statusType);
        alwaysPlusTemp = Mathf.CeilToInt(m_maxValue);
    }

    void CharChanged()
    {
        int currentId = AvatarGroupHelper.GetAvatarBase(SummonerStorage.Instance.LastSummoner);

        id = currentId;
        if (m_headIcon != null) m_headIcon.sprite = AvatarGroupHelper.GetAvatarIcon(id);
        if (m_name != null) m_name.text = LocaleLoader.GetString("CharName" + id.ToString());
        int currentLv = SummonerStorage.Instance.CurrentContent.ExpLevel;
        m_lv.text = "LV. " + currentLv.ToString();

        if (!m_lock) m_maxValue = UIPoolHelper.GetSaveType(m_MaxType);
        m_scale = Mathf.Clamp(m_maxValue, m_minWidth, m_maxWidth) / m_maxValue;
        if (m_sizeControl != null) SetWidth(m_sizeControl, m_maxValue);

        m_statusValue = UIPoolHelper.GetSaveType(m_statusType);
        m_costValue = m_statusValue;
        if (m_costImage != null) SetWidth(m_costImage.rectTransform, m_costValue);
        if (m_statusImage != null) SetWidth(m_statusImage.rectTransform, m_statusValue);
    }
    */
}
