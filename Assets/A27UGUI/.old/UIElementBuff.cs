﻿using UnityEngine;
using UnityEngine.UI;

public class UIElementBuff : MonoBehaviour
{
    [SerializeField] Image m_icon;
    [SerializeField] Image m_timer;
    [SerializeField] Text m_value;

    private UIModelAura.BuffValueType buffType;
    private float startTime = -1f;
    private float endTime = -1f;
    private float TotalTime { get { return endTime - startTime; } }
    private float ExcessTime { get { return endTime - Time.time; } }
    private float Amount { get { return ExcessTime / TotalTime; } }

    public void InitBuff(float a_endTime, UIModelAura.BuffSetting a_buff)
    {
        buffType = a_buff.valueType;
        if (a_buff.icon != null) m_icon.sprite = a_buff.icon;
    }
    public void SetValue(float a_endTime)
    {
        startTime = Time.time;
        endTime = a_endTime;
    }
    private void Update()
    {
        switch (buffType)
        {
            case UIModelAura.BuffValueType.Timer:
                if (startTime <= 0) return;
                if (endTime <= 0) return;
                m_value.text = Mathf.CeilToInt(ExcessTime).ToString("0");
                m_timer.fillAmount = Amount;
                break;

            case UIModelAura.BuffValueType.Counter:
                m_value.text = Mathf.CeilToInt(endTime).ToString("0");
                m_timer.fillAmount = 1f;
                break;

            case UIModelAura.BuffValueType.Switch:
                m_value.text = "";
                m_timer.fillAmount = 1f;
                break;
        }
    }
}
