﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[Serializable]
public class ControllerIconSet
{
    public CrossInputType Key;
    public Image Icon;
}

public class UIControllerIcon : UIElementTipBase
{
    DeviceType mControllerType = DeviceType.none;
    public List<ControllerIconSet> ControllerIconSets;

    void Update()
    {
        if (mControllerType == deviceType)
        {
            return;
        }
        mControllerType = deviceType;

        foreach (var set in ControllerIconSets)
        {
            set.Icon.enabled = false;
        }

        foreach (var set in ControllerIconSets)
        {
            var key = set.Key.ToString();

            if (mControllerType == DeviceType.mouse ||
                mControllerType == DeviceType.keyboard)
            {
                if (kb.ContainsKey(key))
                {
                    var command = kb[key];
                    if (kbdDic.ContainsKey(command))
                    {
                        set.Icon.sprite = kbdDic[command];
                        set.Icon.SetNativeSize();
                        set.Icon.enabled = true;
                    }
                }
                else if (mb.ContainsKey(key))
                {
                    var command = mb[key].ToString();
                    if (mseDic.ContainsKey(command))
                    {
                        set.Icon.sprite = mseDic[command];
                        set.Icon.SetNativeSize();
                        set.Icon.enabled = true;
                    }
                }
            }
            else if (pb.ContainsKey(key))
            {
                var command = pb[key].ToString();
                if (padDic.ContainsKey(command))
                {
                    set.Icon.sprite = padDic[command];
                    set.Icon.SetNativeSize();
                    set.Icon.enabled = true;
                }
            }
        }
    }
}
