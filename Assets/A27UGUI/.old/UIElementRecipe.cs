﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections.Generic;

public class UIElementRecipe : UIElementBrand
{
    [Serializable]
    struct MatUI
    {
        public GameObject group;
        public Image icon;
        public Text name;
        public Text count;
    }
    [Serializable]
    struct CostUI
    {
        public GameObject group;
        public Text count;
    }

    Recipe m_recipe;
    bool canUse = true;

    [SerializeField] Text exchangeType;
    [SerializeField] MatUI[] srcMats = new MatUI[3];
    [SerializeField] CostUI[] srcCosts = new CostUI[5];
    [SerializeField] MatUI[] tgtMats = new MatUI[3];
    [SerializeField] CostUI[] tgtCosts = new CostUI[5];

    public void Set(Recipe a_recipe, UIElementPage a_manager)
    {
        m_listManager = a_manager;
        m_recipe = a_recipe;

        exchangeType.text = a_recipe.type.ToString();

        SetMat(a_recipe.srcMat_1, srcMats[0], true);
        SetMat(a_recipe.srcMat_2, srcMats[1], true);
        SetMat(a_recipe.srcMat_3, srcMats[2], true);
        SetCost(a_recipe.srcCost_1, srcCosts, true);
        SetCost(a_recipe.srcCost_2, srcCosts, true);
        GetComponent<Selectable>().interactable = canUse;

        SetMat(a_recipe.tgtMat_1, tgtMats[0]);
        SetMat(a_recipe.tgtMat_2, tgtMats[1]);
        SetMat(a_recipe.tgtMat_3, tgtMats[2]);
        SetCost(a_recipe.tgtCost_1, tgtCosts);
        SetCost(a_recipe.tgtCost_2, tgtCosts);
    }

    Dictionary<string, string> paths = new Dictionary<string, string>();
    string GetPath(string a_type)
    {
        if (!paths.ContainsKey(a_type))
            paths.Add(a_type, typeof(ResourcesHelper).GetField(a_type.ToUpper() + "_ICON").GetValue(null) as string);
        return paths[a_type];
    }

    void SetMat(RecipeMat a_mat, MatUI a_ui, bool a_check = false)
    {
        a_ui.group.SetActive(a_mat.count > 0);
        if (a_mat.count <= 0) return;

        if (a_check && CheckMat(a_mat) == false)
        {
            canUse = false;
            a_ui.count.color = ColorHelper.ERROR;
            a_ui.icon.color = ColorHelper.ERROR;
            a_ui.name.color = ColorHelper.ERROR;
        }

        string name = "";
        Sprite icon = null;
        string cnt = "";
        switch (a_mat.type)
        {
            case PickupType.Item:
                icon = Resources.Load<Sprite>(GetPath("Item") + a_mat.id);
                name = LocaleLoader.GetString("ItemName" + a_mat.id);
                var siia = ItemBag.Instance.m_shortcutItemList.Find(x => x.ID == a_mat.id);
                int amount = siia == null ? 0 : siia.amount;
                cnt = a_mat.count.ToString() + " / " + amount;
                break;
            case PickupType.Equip:
                icon = Resources.Load<Sprite>(GetPath("Equip") + a_mat.id);
                name = LocaleLoader.GetString("EquipName" + a_mat.id);
                break;
            case PickupType.GodGift:
                icon = Resources.Load<Sprite>(GetPath("Gift") + a_mat.id);
                name = LocaleLoader.GetString("GiftName" + a_mat.id);
                break;
        }
        a_ui.icon.sprite = icon;
        a_ui.name.text = name;
        a_ui.count.text = cnt;
    }

    private bool CheckMat(RecipeMat a_mat)
    {
        bool result = true;
        if (a_mat.count < 1) result = false;
        switch (a_mat.type)
        {
            case PickupType.GodGift:
                if (!Recipe.GiftList.Contains(a_mat.id)) result = false;
                break;
            case PickupType.Equip:
                var gse = SummonerStorage.Instance.CurrentContent.m_gachaSave.Exists(
                    x => x.m_type == GachaType.Equipment && x.m_id == a_mat.id);
                if (gse == false) result = false;
                break;
            case PickupType.Item:
                var gsi = SummonerStorage.Instance.CurrentContent.m_gachaSave.Exists(
                    x => x.m_type == GachaType.Item && x.m_id == a_mat.id && x.param >= a_mat.count);
                if (gsi == false) result = false;
                break;
        }

        return result;
    }

    void SetCost(RecipeCost a_cost, CostUI[] a_uis, bool a_check = false)
    {
        if (a_cost.count > 0 && a_cost.type != RecipeCostType.none)
        {
            int index = (int)a_cost.type - 1;
            int cnt = 0;
            switch (a_cost.type)
            {
                case RecipeCostType.Essence:
                    cnt = SummonerStorage.Instance.Soul;
                    break;
                case RecipeCostType.Key:
                    cnt = ItemBag.Instance.m_itemList.FindAll(x => x == 2000).Count;
                    break;
                case RecipeCostType.Crystal:
                    cnt = SummonerStorage.Instance.CurrentContent.m_dropSoulCrystals.FindAll(x => x.Drop == 3000).Count;
                    break;
                case RecipeCostType.RareCrystal:
                    cnt = SummonerStorage.Instance.CurrentContent.m_dropSoulCrystals.FindAll(x => x.Drop == 3001).Count;
                    break;
                case RecipeCostType.AncientCrystal:
                    cnt = SummonerStorage.Instance.CurrentContent.m_dropSoulCrystals.FindAll(x => x.Drop == 3002).Count;
                    break;
            }
            CostUI ui = a_uis[index];
            if (a_check && cnt < a_cost.count)
            {
                canUse = false;
                ui.count.color = ColorHelper.ERROR;
            }
            ui.group.SetActive(true);
            ui.count.text = a_cost.count + " / " + cnt;
        }
    }

    public new void OnClick()
    {
        m_recipe.Cook();
        base.OnClick();
    }
}
