﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class UIElementTipBase : MonoBehaviour
{
    [System.Serializable]
    public struct TipSet
    {
        public CrossInputType type;
        public Image mouseIcon;
        public Image padIcon;
    }

    protected static List<UIElementTip> tips = new List<UIElementTip>();
    protected static DeviceType deviceType = DeviceType.none;
    public static DeviceType DeviceType
    {
        get { return deviceType; }
        set
        {
            if (value != deviceType)
            {
                bool mouse = value == DeviceType.keyboard || value == DeviceType.mouse;
                if (IsUseMouse & mouse == true) return;
                IsUseMouse = mouse;
                Cursor.visible = IsUseMouse;

                deviceType = value;
                if ((isPs && value == DeviceType.xbox) || (!isPs && value == DeviceType.ps))
                {
                    RefreshAllIcons();
                    isPs = value == DeviceType.ps;
                }

                SwitchAllIcons();
            }
        }
    }
    public static bool IsUseMouse { get; private set; }
    public static bool isPs { get; private set; }

    private static Dictionary<DeviceType, Dictionary<string, Sprite>> iconSet = null;
    public static Dictionary<DeviceType, Dictionary<string, Sprite>> IconSet
    {
        get
        {
            if (iconSet == null)
            {
                var setting = Resources.Load<ControlIconObject>(@"ScriptableObject/ControlIconSetting");
                iconSet = new Dictionary<DeviceType, Dictionary<string, Sprite>>();
                foreach (var d in setting.devices)
                {
                    var dic = new Dictionary<string, Sprite>();
                    foreach (var ci in d.icons)
                    {
                        if (dic.ContainsKey(ci.name))
                        {
                            Debug.Log(d.type + " already contains key : " + ci.name);
                            continue;
                        }
                        dic.Add(ci.name, ci.icon);
                    }
                    iconSet.Add(d.type, dic);
                }
            }
            return iconSet;
        }
    }

    protected static Dictionary<string, Sprite> padDic, kbdDic, mseDic;
    protected static Dictionary<string, InControl.InputControlType> pb;
    protected static Dictionary<string, string> kb;
    protected static Dictionary<string, InControl.Mouse> mb;
    protected static void RefreshDic()
    {
        DeviceType pd = deviceType == DeviceType.ps ? DeviceType.ps : DeviceType.xbox;
        padDic = IconSet[pd];
        kbdDic = IconSet[DeviceType.keyboard];
        mseDic = IconSet[DeviceType.mouse];
        pb = CrossInputBindCtrl.GetJoystickBinding();
        kb = CrossInputBindCtrl.GetKeyboardBinding();
        mb = CrossInputBindCtrl.GetMouseBinding();
    }

    protected virtual void SwitchIcons() { }
    public static void SwitchAllIcons()
    {
        tips.RemoveAll(x => x == null);
        foreach (UIElementTip t in tips)
        {
            t.SwitchIcons();
        }
    }

    public virtual void RefreshIcons(bool a_callFromBase) { }
    public static void RefreshAllIcons()
    {
        //Debug.Log("RefreshAllIcons()");
        RefreshDic();

        tips.Clear();
        UIElementTip[] ts = Panel.Canvas.GetComponentsInChildren<UIElementTip>(true);
        tips.AddRange(ts);

        foreach (UIElementTip t in tips)
        {
            t.RefreshIcons(true);
        }
    }
}
