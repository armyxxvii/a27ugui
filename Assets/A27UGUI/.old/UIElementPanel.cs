﻿using System.Collections;
using UnityEngine;

public class UIElementPanel : MonoBehaviour
{
    [System.Serializable]
    public struct State
    {
        public bool active;
        public bool interactable;
        public float duration;
        [Range(0, 1.0f)]
        public float alpha;
        public Vector2 pos;
        public Vector2 size;
        public Vector3 scale;
    }

    [SerializeField] CanvasGroup m_cGroup;
    public CanvasGroup CanvasGroup { get { return m_cGroup; } }
    [SerializeField] RectTransform m_rTransform;
    [SerializeField] bool m_switchActive;
    public bool SwitchActive { get { return m_switchActive; } }
    [SerializeField] bool m_switchInteractable;
    public bool SwitchInteractable { get { return m_switchInteractable; } }
    [SerializeField] bool m_fade;
    [SerializeField] bool m_move = true;
    [SerializeField] State[] m_states = new State[2];
#if UNITY_EDITOR
    public int StateCount { get { return m_states.Length; } }
    [ContextMenuItem("Get Transform", "Get")]
    [ContextMenuItem("Switch to", "SwitchN")]
    public int WorkState = 0;
    void SwitchN() { Switch(WorkState); }
    void Get()
    {
        if (WorkState < 0 || WorkState >= m_states.Length) return;
        m_states[WorkState].pos = m_rTransform.anchoredPosition;
        m_states[WorkState].size = m_rTransform.sizeDelta;
    }
#endif

    private void Start()
    {
        if (m_cGroup == null)
            m_cGroup = GetComponent<CanvasGroup>();
        if (m_rTransform == null)
            m_rTransform = GetComponent<RectTransform>();
    }

    bool Active(int a_index) { return m_states[a_index].active; }
    bool Interactable(int a_index) { return m_states[a_index].interactable; }
    float Duration(int a_index) { return m_states[a_index].duration; }
    float Alpha(int a_index) { return m_states[a_index].alpha; }
    Vector2 Pos(int a_index) { return m_states[a_index].pos; }
    Vector2 Size(int a_index) { return m_states[a_index].size; }
    Vector3 Scale(int a_index) { return m_states[a_index].scale; }

    IEnumerator Switching(int a_index)
    {
        if (m_move || m_fade)
        {
            float start = Time.time;
            float end = start + Duration(a_index);
            var oldAP = m_rTransform.anchoredPosition;
            var oldSD = m_rTransform.sizeDelta;
            var oldLS = m_rTransform.localScale;
            var oldA = m_cGroup.alpha;
            while (Time.time < end)
            {
                float t = (Time.time - start) / Duration(a_index);
                if (m_move)
                {
                    m_rTransform.anchoredPosition = Vector2.Lerp(oldAP, Pos(a_index), t);
                    m_rTransform.sizeDelta = Vector2.Lerp(oldSD, Size(a_index), t);
                    m_rTransform.localScale = Vector3.Lerp(oldLS, Scale(a_index), t);
                }
                if (m_fade) m_cGroup.alpha = Mathf.Lerp(oldA, Alpha(a_index), t); ;

                yield return null;
            }
            if (m_move)
            {
                m_rTransform.anchoredPosition = Pos(a_index);
                m_rTransform.sizeDelta = Size(a_index);
                m_rTransform.localScale = Scale(a_index);
            }
            if (m_fade) m_cGroup.alpha = Alpha(a_index);
        }

        if (m_switchInteractable)
        {
            m_cGroup.interactable = Interactable(a_index);
            m_cGroup.blocksRaycasts = Interactable(a_index);
        }
        if (m_switchActive) m_rTransform.gameObject.SetActive(Active(a_index));
        yield break;
    }

    public float Switch(int a_index = -1)
    {
        if (a_index < 0 || a_index >= m_states.Length) return 0;

        m_rTransform.gameObject.SetActive(true);
        StopAllCoroutines();
        StartCoroutine(Switching(a_index));
        return m_states[a_index].duration;
    }
}
