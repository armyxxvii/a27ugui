﻿using UnityEngine;
using UnityEngine.UI;
using System;

[Serializable]
public struct BrandData
{
    public int id;
    public int level;
    public int type;
    public Sprite icon;

    public BrandData(int a_id, int a_level, int a_type, Sprite a_icon)
    {
        id = a_id;
        level = a_level;
        type = a_type;
        icon = a_icon;
    }
}

public class UIElementBrand : MonoBehaviour
{
    public enum BrandType
    {
        Seal,
        Ach,
        Cloth,
        Crystal,
        Recipe,
    }

    public int m_id;
    public string m_idStr { get; private set; }
    public BrandType m_brandType;
    public int m_type;
    public Image m_icon;
    public Text m_level;
    public Text m_name;
    public Text m_description;

    private Property property;
    protected UIElementPage m_listManager;

    public void Set(BrandData a_data, UIElementPage a_manager)
    {
        m_listManager = a_manager;

        m_id = a_data.id;
        m_idStr = a_data.id.ToString();
        m_type = a_data.type;

        if (m_level) m_level.text = a_data.level > 0 ? a_data.level.ToString() : "";
        if (m_icon) m_icon.sprite = a_data.icon;

        string bt = m_brandType.ToString();
        
        if (m_name)
            m_name.text = LocaleLoader.GetString(bt + "Name" + m_idStr);

        if (m_description)
        {
            string origin = LocaleLoader.GetString(bt + "Desc" + m_idStr);
            string[] split = origin.Split('<');
            string[] splitNew = (string[])split.Clone();
            int index = 0;
            property = (Property)PropertyLoader.Instance.m_settings[a_data.id];
            for (int i = 0; i < split.Length; i++)
            {
                int lIndex = split[i].IndexOf('>');
                if (lIndex > -1)
                {
                    float value = Mathf.Abs(property.sheets[index].SheetValue(0) * a_data.level);

                    int digits = 0;
                    int dIndex = split[i].IndexOf('.');
                    if (dIndex > -1 && dIndex+1 < split[i].Length)
                    {
                        if (char.IsNumber(split[i][dIndex + 1]) )
                        {
                            digits = split[i][dIndex + 1] - '0';
                        }
                        value /= Mathf.Pow(10, digits);
                    }
                    splitNew[i] = "<color=lime> " + value.ToString() + " </color>" + split[i].Substring(lIndex + 1);
                    index++;
                }
            }
            m_description.text = string.Concat(splitNew);
        }
    }

    public void OnPoint() { if (m_listManager) m_listManager.BrandSelected(this); }
    public void OnClick() { if (m_listManager) m_listManager.BrandClicked(this); }

}
