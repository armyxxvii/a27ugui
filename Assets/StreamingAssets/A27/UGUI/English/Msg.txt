﻿┤UIMsgBoxNameEA:=Welcome to the Neverinth├
┤UIMsgBoxDescEA:=Neverinth is in Early Access, and that means, the game is still under development. What you see is not the final version. In this version, you might find some weird, annoying, and even silly, bugs, but don't worry, we are working on them.

Currently, we are updating the game, debugging, making adjustments, and optimizing to create the best gaming experience possible!  The most important thing is that we are also adding new content! As you can see, the project is grand and we are still some way from completing it! 

If you find a bug, or have any feedback or idea, please contact us in the Steam Forums and at discord.gg/ai. Any comment is welcome and all will help us make Neverinth even better. You can also find us on Twitter: @Neverinth and Facebook. 

May Hamingja be with you!

CreAct Game
Discord：Discord.gg/ai〈Neverinth〉
Email：creactgames@gmail.com├
┤UIMsgBoxNameThank:=Thanks for playing Neverinth├
┤UIMsgBoxDescThank:=Congratulations for completing all the content we’ve designed so far, including the parts that are still being developed.

True, we, CreAct Game, are a small indie team, but rest assured that we are working on creating more content every day

If you like Neverinth, please help us by telling your friends about our game! Any feedback or comments are welcomed.  Thanks for playing Neverinth. If you have more feedback, you can reach on Social media and Discord

CreAct Game├
┤UIMsgBoxNameContinue:=To Be Continue…├
┤UIMsgBoxDescContinue:=├
┤UIMsgBoxNameWork:=Under Development├
┤UIMsgBoxDescWork:=Oops… you have completed all content currently available in Neverinth so far, and we are working really hard to expand Neverinth. 

If you’d like to play the whole game, without spoilers, please go to menu to exit the game and return to the Hall of Trial.

If you take great interest in Neverinth and would like to know more, we’ve prepared a small surprise for you at the end, along with with a teeny-weeny challenge. Thanks for playing Neverinth.├
┤UIMsgBoxNameDestroyCrystal:=You will lose this crystal forever!
Do you really want to destroy this crystal?├
┤UIMsgBoxDescDestroyCrystal:=├
┤UIMsgBoxNameApplyOption:=System setting applied├
┤UIMsgBoxDescApplyOption:=├
┤UIMsgBoxNameDefOption:=The setting will return to default
Are you sure?├
┤UIMsgBoxDescDefOption:=├
┤UIMsgBoxNameInputConflict:=Control setting conflict├
┤UIMsgBoxDescInputConflict:=├
┤UIMsgBoxNameExitGame:=Exit the game├
┤UIMsgBoxDescExitGame:=├
┤UIMsgBoxNameExitLobby:=All temporary abilities you gain will be lost, but your character Level and Crystal Energy will be retained.
Are you sure you want to return to the Hall of Trial?├
┤UIMsgBoxDescExitLobby:=├
┤UIMsgBoxNameDelSave:=Do you really want to delete your saved records?├
┤UIMsgBoxDescDelSave:=├
┤UIMsgBoxNameLockedTotem:=Totem device deactivated├
┤UIMsgBoxDescLockedTotem:=├
┤UIMsgBoxNameCredit:=Credit├
┤UIMsgBoxDescCredit:=
- CreAct Game -
Alex Tsai / Producer
RVX  / Game director / Animation designer  / Level designer / Animator
Carol Liu / Art director / Community manager
Wu Shun Feng / Programmer
Ssu Hsin Huang / Programmer
27 Army  / Programmer / Technical artist
Chen I Ching / Character & environment artist  / Level designer
Eddie Lin / VFX Artist / IT
Jennie Hsu / Animator / Rigging
Lucien Chen / SFX designer

- Former teammates -
Wu jhih wei/programmer、Bruce Chou/artist、Jeffr/character artist、jeson110/character artist、Jiang Yan/3d artist、Yin-Zhen Xue/VFX Artist 

- External assistance -
B2、fanyi4life、Cheng Chia Ju、Roger Lo、Jouni Valjakka

- Special Thanks -
Jonas Bötel、LU、Furukawa Hiroki

- Free Music Resources -
"DRAFTY PLACES" by Eric Matyas  [ www.soundimage.org ]
"Chaos and Despair"、"Return to Darkness" by YouFulca  [ wingless-seraph.net ]

- Free Vocal Resources -
AderuMoro  [ freesound.org/people/AderuMoro/ ]
AmeAngelofSin  [ freesound.org/people/AmeAngelofSin/ ]├
┤UIMsgBoxNameWarning:=Important Announcement├
┤UIMsgBoxDescWarning:=├
┤UIMsgBoxNameStrengthenTotem:=Totem equipped!
Do you want to strengthen this totem?├
┤UIMsgBoxDescStrengthenTotem:=├
