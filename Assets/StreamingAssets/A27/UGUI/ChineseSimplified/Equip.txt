﻿┤EquipName1:=初始之剑├
┤EquipPerf1:=物理攻击 +100├
┤EquipDesc1:=初阶武器，没有任何附加能力├
┤EquipName2:=斗士之剑├
┤EquipPerf2:=物理攻击 +125├
┤EquipDesc2:=所有攻击精力消耗增加15%
白血回复速度提升40%├
┤EquipName3:=敏锐细剑├
┤EquipPerf3:=物理攻击 +105├
┤EquipDesc3:=所有攻击精力消耗减少15%
中断值降低20%
任何动作造成的白血消耗减少30%
稍微加快攻击动作├
┤EquipName4:=重石之剑├
┤EquipPerf4:=物理攻击 +150├
┤EquipDesc4:=闪避、EX闪避精力消耗增加25%
EX攻击精力消耗增加40%├
┤EquipName5:=增幅之剑├
┤EquipPerf5:=物理攻击 +135├
┤EquipDesc5:=元素类恩惠伤害提升50%
空间类伤害提升75%├
┤EquipName6:=狂袭之剑├
┤EquipPerf6:=物理攻击 +130├
┤EquipDesc6:=白血状态中，物理伤害提升25%├
┤EquipName7:=守护项链├
┤EquipPerf7:=护盾值+120
物理防御 +5├
┤EquipDesc7:=受到伤害后0.7秒内，再次受到的伤害减少50%├
┤EquipName8:=偏移项链├
┤EquipPerf8:=护盾值+160
物理防御 +3├
┤EquipDesc8:=飞行物造成的伤害减少75%├
┤EquipName9:=毅力项链├
┤EquipPerf9:=护盾值+90
物理防御 +9├
┤EquipDesc9:=坚韧值回复速度提升50%├
┤EquipName10:=忍耐项链├
┤EquipPerf10:=护盾值+200
物理防御 +8├
┤EquipDesc10:=白血状态中，受到的伤害减少40%├
┤EquipName11:=抗魔项链├
┤EquipPerf11:=护盾值+130
物理防御 +12├
┤EquipDesc11:=元素、燃烧、中毒造成的伤害减少50%├
┤EquipName12:=炼金戒指├
┤EquipPerf12:=护盾值+150├
┤EquipDesc12:=使用道具，有20%的机率不会消耗├
┤EquipName13:=复活戒指├
┤EquipPerf13:=护盾值+80├
┤EquipDesc13:=死亡之后，血量全满原地复活，但是戒指会毁坏├
┤EquipName14:=巧手戒指├
┤EquipPerf14:=护盾值+140
物理防御 +4├
┤EquipDesc14:=使用道具的速度提升30%├
┤EquipName15:=锁力戒指├
┤EquipPerf15:=护盾值+90
物理防御 +3├
┤EquipDesc15:=玩家每持有1把钥匙，道具伤害提升10%├
┤EquipName16:=潜行戒指├
┤EquipPerf16:=护盾值+75
物理防御 +2├
┤EquipDesc16:=玩家移动声音降低100%├
┤EquipName17:=速刃耳环├
┤EquipPerf17:=护盾值+130├
┤EquipDesc17:=护盾存在时，EX攻击伤害提升70%├
┤EquipName18:=变则耳环├
┤EquipPerf18:=护盾值+200├
┤EquipDesc18:=护盾存在时，每持有300晶髓，提升物理伤害1%├
┤EquipName19:=猎血耳环├
┤EquipPerf19:=护盾值+160├
┤EquipDesc19:=周围敌人受伤时，受伤的10%转换为自身血量├
┤EquipName20:=巧手耳环├
┤EquipPerf20:=护盾值+250├
┤EquipDesc20:=护盾值存在时，道具伤害提升30%├
┤EquipName21:=顽强耳环├
┤EquipPerf21:=护盾值+120├
┤EquipDesc21:=白血回复速度提升50%├
┤EquipName22:=初始大剑├
┤EquipPerf22:=物理攻击 +100├
┤EquipDesc22:=初阶武器，没有任何附加能力├
┤EquipName23:=斗士大剑├
┤EquipPerf23:=物理攻击 +125├
┤EquipDesc23:=所有攻击精力消耗增加15%
白血回复速度提升40%├
┤EquipName24:=尖敏大剑├
┤EquipPerf24:=物理攻击 +105├
┤EquipDesc24:=所有攻击精力消耗减少15%
中断值降低20%
任何动作造成的白血消耗减少30%
稍微加快攻击动作├
┤EquipName25:=重石大剑├
┤EquipPerf25:=物理攻击 +150├
┤EquipDesc25:=闪避、EX闪避精力消耗增加25%
EX攻击精力消耗增加40%├
┤EquipName26:=增幅大剑├
┤EquipPerf26:=物理攻击 +135├
┤EquipDesc26:=元素类恩惠伤害提升50%
空间类伤害提升75%├
┤EquipName27:=狂袭大剑├
┤EquipPerf27:=物理攻击 +130├
┤EquipDesc27:=白血状态中，物理伤害提升25%├
┤EquipName28:=勇气武技大剑├
┤EquipPerf28:=物理攻击 +145├
┤EquipDesc28:=武器特殊动作 伤害提升50%
白血回复速度降低35%├
┤EquipName29:=远古战损大剑├
┤EquipPerf29:=物理攻击 +165├
┤EquipDesc29:=EX攻击、武器技能、武器特殊动作 的伤害降低40%
闪避、EX闪避精力消耗增加30%├
┤EquipName30:=野蛮狂袭大剑├
┤EquipPerf30:=物理攻击 +170├
┤EquipDesc30:=所有攻击精力消耗增加20%
任何动作造成的白血消耗增加60%
白血状态中，伤害提升20%├
┤EquipName31:=密勇心技大剑├
┤EquipPerf31:=物理攻击 +140├
┤EquipDesc31:=攻击的中断值提升30%
闪避、EX闪避精力消耗减少50%├
┤EquipName32:=无名王者大剑├
┤EquipPerf32:=物理攻击 +190├
┤EquipDesc32:=任何动作造成的白血消耗减少40%
EX攻击、EX闪避精力消耗减少25%├
┤EquipName33:=勇气武技之剑├
┤EquipPerf33:=物理攻击 +145├
┤EquipDesc33:=武器特殊动作 伤害提升50%
白血回复速度降低35%├
┤EquipName34:=远古战损之剑├
┤EquipPerf34:=物理攻击 +165├
┤EquipDesc34:=EX攻击、武器技能、武器特殊动作 的伤害降低40%
闪避、EX闪避精力消耗增加30%├
┤EquipName35:=不动之剑├
┤EquipPerf35:=物理攻击 +150├
┤EquipDesc35:=攻击墙壁或是重盾将不会弹刀
抵挡攻击时，精力消耗降低25%├
┤EquipName36:=密勇心技弯剑├
┤EquipPerf36:=物理攻击 +140├
┤EquipDesc36:=攻击的中断值提升30%
闪避、EX闪避精力消耗减少50%├
┤EquipName37:=圣纹王家之剑├
┤EquipPerf37:=物理攻击 +190├
┤EquipDesc37:=任何动作造成的白血消耗减少40%
EX攻击、EX闪避精力消耗减少25%├
┤EquipName38:=勉力耳环├
┤EquipPerf38:=护盾值+135├
┤EquipDesc38:=精力每消耗3点，伤害提升1%├
┤EquipName39:=心力耳环├
┤EquipPerf39:=护盾值+170├
┤EquipDesc39:=每消耗一次精力,消耗前的精力值除以10，转为伤害%数├
┤EquipName40:=神力耳环├
┤EquipPerf40:=护盾值+150├
┤EquipDesc40:=护盾存在时，伤害提升30%├
┤EquipName41:=潜力耳环├
┤EquipPerf41:=护盾值+70
物理防御 +10├
┤EquipDesc41:=血量少于25%时，伤害提升25%├
┤EquipName42:=痛击耳环├
┤EquipPerf42:=护盾值+90
物理防御 +5├
┤EquipDesc42:=当敌人在异常状态下(燃烧、带电、冰冻、中毒、麻痹、晕眩、缓慢)，对目标伤害提升20%├
┤EquipName43:=锁御戒指├
┤EquipPerf43:=护盾值+110
物理防御 +6├
┤EquipDesc43:=玩家每持有1把钥匙，防御力提升2%├
┤EquipName44:=巧御戒指├
┤EquipPerf44:=护盾值+100
物理防御 +3├
┤EquipDesc44:=使用道具动作中，防御力提升100%，且动作不会被中断├
┤EquipName45:=蛮王戒指├
┤EquipPerf45:=护盾值+50
物理防御 +1├
┤EquipDesc45:=武器的攻击力+30%
武器的能力数值加倍├
┤EquipName46:=连结戒指├
┤EquipPerf46:=护盾值+110
物理防御 +10├
┤EquipDesc46:=耳环的能力数值加倍├
┤EquipName47:=洞察项链├
┤EquipPerf47:=护盾值+140
物理防御 +7├
┤EquipDesc47:=机关造成的伤害减少75%├
┤EquipName48:=猫之项链├
┤EquipPerf48:=护盾值+120
物理防御 +10├
┤EquipDesc48:=摔落造成的伤害减少95%├
┤EquipName49:=活力项链├
┤EquipPerf49:=护盾值+200
物理防御 +12├
┤EquipDesc49:=提升精力回复速度30%├
┤EquipName50:=冻结项链├
┤EquipPerf50:=护盾值+120
物理防御 +8├
┤EquipDesc50:=受到的伤害增加35%，并冰冻近距离的攻击对象├
┤EquipName51:=完抗项链├
┤EquipPerf51:=护盾值+140
物理防御 +10├
┤EquipDesc51:=所有异常状态免疫(燃烧、带电、冰冻、中毒、麻痹、晕眩、缓慢)├
┤EquipName52:=无暇耳环├
┤EquipPerf52:=护盾值+120├
┤EquipDesc52:=血量愈多，攻击伤害愈高

(依照剩余血量百分比增加攻击伤害%数，最多增加25%)├
┤EquipName53:=回复项链├
┤EquipPerf53:=护盾值+110
物理防御 +8├
┤EquipDesc53:=道具的补血回复量+50%├
┤EquipName54:=奋力耳环├
┤EquipPerf54:=护盾值+100├
┤EquipDesc54:=精力愈少，攻击伤害愈高

(依照剩余精力百分比增加攻击伤害%数，最多增加25%)├
┤EquipName55:=初始武士刀├
┤EquipPerf55:=物理攻击 +100├
┤EquipDesc55:=初阶武器，没有任何附加能力├
┤EquipName56:=斗士之刀├
┤EquipPerf56:=物理攻击 +125├
┤EquipDesc56:=所有攻击精力消耗增加15%
白血回复速度提升40%├
┤EquipName57:=敏锐武士刀├
┤EquipPerf57:=物理攻击 +105├
┤EquipDesc57:=所有攻击精力消耗减少15%
中断值降低20%
任何动作造成的白血消耗减少30%
稍微加快攻击动作├
┤EquipName58:=重石武士刀├
┤EquipPerf58:=物理攻击 +150├
┤EquipDesc58:=闪避、EX闪避精力消耗增加25%
EX攻击精力消耗增加40%├
┤EquipName59:=增幅武士刀├
┤EquipPerf59:=物理攻击 +135├
┤EquipDesc59:=元素类恩惠伤害提升50%
空间类伤害提升75%├
┤EquipName60:=狂袭武士刀├
┤EquipPerf60:=物理攻击 +130├
┤EquipDesc60:=白血状态中，物理伤害提升25%├
┤EquipName61:=勇气武技之刀├
┤EquipPerf61:=物理攻击 +145├
┤EquipDesc61:=武器特殊动作 伤害提升50%
白血回复速度降低35%├
┤EquipName62:=远古战损之刀├
┤EquipPerf62:=物理攻击 +165├
┤EquipDesc62:=EX攻击、武器技能、武器特殊动作 的伤害降低40%
闪避、EX闪避精力消耗增加30%├
┤EquipName63:=野蛮狂袭之刀├
┤EquipPerf63:=物理攻击 +170├
┤EquipDesc63:=所有攻击精力消耗增加20%
任何动作造成的白血消耗增加60%
白血状态中，伤害提升20%├
┤EquipName64:=密勇心技之刀├
┤EquipPerf64:=物理攻击 +140├
┤EquipDesc64:=攻击的中断值提升30%
闪避、EX闪避精力消耗减少50%├
┤EquipName65:=无名将军之刀├
┤EquipPerf65:=物理攻击 +190├
┤EquipDesc65:=任何动作造成的白血消耗减少40%
EX攻击、EX闪避精力消耗减少25%├
┤EquipName66:=27件雕塑刀组├
┤EquipPerf66:=物理攻击+1├
┤EquipDesc66:=使用道具不再消耗
所有物理攻击伤害降低999%
精力消耗增加25%
抵挡攻击时，精力消耗降低45％├
┤EquipName67:=RVX 大人的超速之剑├
┤EquipPerf67:=物理攻击 +160├
┤EquipDesc67:=大幅加速角色的攻击速度
攻击不再消耗精力├
┤EquipName68:=RVX 大人的超速大剑├
┤EquipPerf68:=物理攻击 +160├
┤EquipDesc68:=大幅加速角色的攻击速度
攻击不再消耗精力├
┤EquipName69:=RVX 大人的超速之刀├
┤EquipPerf69:=物理攻击 +160├
┤EquipDesc69:=大幅加速角色的攻击速度
攻击不再消耗精力├
┤EquipName70:=暴发户静的贪婪├
┤EquipPerf70:=护盾值+150
物理防御 +5├
┤EquipDesc70:=获得5倍的晶髓├
┤EquipName71:=雷米公爵的为所欲为├
┤EquipPerf71:=护盾值+100├
┤EquipDesc71:=使用任何动作不再消耗白血
闪避动作完全无敌├
┤EquipName72:=ZG爵士的礼物├
┤EquipPerf72:=护盾值+70
物理防御 +2├
┤EquipDesc72:=闪避后，原地放置一个小炸药桶├
┤EquipName73:=小心我的加倍奉还├
┤EquipPerf73:=护盾值+100├
┤EquipDesc73:=受到的伤害数值，转为攻击伤害增加，持续累积20秒
若持有 猎血耳环 或 歌萝特图腾，降低其 [周围敌人受伤时，受伤的%转换为自身血量] 的能力├
┤EquipName74:=扁头的一击├
┤EquipPerf74:=物理防御 +200├
┤EquipDesc74:=闪避后，伤害提升150%，受到的伤害增加50%，直到 攻击敌人 或 受到伤害 为止├
┤EquipName75:=凯若女王的心想事成├
┤EquipPerf75:=护盾值+100
物理防御 +20├
┤EquipDesc75:=女神的祝福时，提高所有符文取得率70%├
┤EquipName76:=符文的倍返├
┤EquipPerf76:=护盾值+120
物理防御 +7├
┤EquipDesc76:=符文等级提升2倍├
┤EquipName77:=不动如山的珍妮女爵├
┤EquipPerf77:=护盾值+10
物理防御 +50├
┤EquipDesc77:=受击不再出现受击动作
每1滴白血，增加1%伤害
受到伤害不再扣血，转换为精力消耗，精力一旦为0，再次受到伤害立即死亡├
