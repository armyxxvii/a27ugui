using UnityEditor;

namespace A27.UGUI
{
    public static class A27UGUI_MenuItems
    {
        [MenuItem("Tools/A27/UGUI/Add LocaleLoader")]
        public static void AddLoader()
        {
            LocaleLoader.AddLoader();
        }
        [MenuItem("Tools/A27/UGUI/Translate Canvas")]
        public static void Translate()
        {
            LocaleLoader.Translate();
        }
        [MenuItem("Tools/A27/UGUI/Update all A27Image's Locale Sprite Set")]
        public static void UpdateAllImages()
        {
            LocaleLoader.UpdateAllImages();
        }

        //[MenuItem("Tools/A27/UGUI/Create Setting", true)]
        //static bool CheckSetting() { return LocaleLoader.Setting == null; }
        //[MenuItem("Tools/A27/UGUI/Create Setting")]
        //public static void CreateSetting()
        //{
        //    if (!File.Exists(LocaleLoader.SettingData.Path))
        //        LocaleLoader.Setting.Save();
        //}

        [MenuItem("Tools/A27/UGUI/Edit Setting &1", priority = 10000)]
        public static void EditSetting() { EditSystem_Window.ShowWindow(); }

        [MenuItem("Tools/A27/UGUI/Edit User Setting &2", priority = 10000)]
        public static void EditUserSetting() { EditUser_Window.ShowWindow(); }
    }
}