using UnityEngine;
using UnityEditor;

namespace A27.UGUI
{
    public class EditUser_Window : EditorWindow
    {
        static EditUser_Window instance;
        static Rect winPos = Rect.zero;

        private Vector2 scrollPos;
        public UserData workSetting;
        private SerializedObject serializedObject;

        public static void ShowWindow()
        {
            if (instance != null) return;
            instance = CreateInstance<EditUser_Window>();
            instance.titleContent = new GUIContent("User Data");
            if (winPos == Rect.zero)
            {
                var s = EditorGUIUtility.GetMainWindowPosition();
                Vector2 size = new Vector2(400f, s.height * 0.5f);
                Vector2 pos = new Vector2(s.x + (s.width - size.x) * 0.5f, s.y + (s.height - size.y) * 0.5f);
                winPos = new Rect(pos, size);
            }
            instance.position = winPos;
            instance.ShowUtility();
        }

        private void OnEnable()
        {
            workSetting = new UserData();
            workSetting.Load();
            serializedObject = new SerializedObject(this);
            //Debug.Log("SettingEdit_Window.Enable");
        }

        private void OnDisable()
        {
            serializedObject.Dispose();
            //Debug.Log("SettingEdit_Window.Disable");
        }

        private void OnGUI()
        {
            serializedObject.Update();

            using (var a = new EditorGUILayout.ScrollViewScope(scrollPos))
            {
                scrollPos = a.scrollPosition;
                var iter = serializedObject.FindProperty("workSetting");
                iter.NextVisible(true);
                do { EditorGUILayout.PropertyField(iter, true); }
                while (iter.NextVisible(false));
            }
            EditorGUILayout.Separator();

            using (var h = new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Reset"))
                {
                    workSetting.Load();
                    return;
                }
                if (GUILayout.Button("Save"))
                {
                    workSetting.Save();
                    winPos = position;
                    Close();
                    ShowWindow();
                    return;
                }
                if (GUILayout.Button("Cancel"))
                {
                    Close();
                    winPos = Rect.zero;
                    return;
                }
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}