using UnityEditor;
using UnityEngine;

namespace A27.UGUI
{
    public class EditSystem_Window : EditorWindow
    {
        static EditSystem_Window instance;
        static Rect winPos = Rect.zero;

        private Vector2 scrollPos;
        public SystemData workSetting;
        private SerializedObject serializedObject;

        public static void ShowWindow()
        {
            instance = CreateInstance<EditSystem_Window>();
            instance.titleContent = new GUIContent("Locale Setting");
            if (winPos == Rect.zero)
            {
                var s = EditorGUIUtility.GetMainWindowPosition();
                Vector2 size = new Vector2(400f, s.height * 0.5f);
                Vector2 pos = new Vector2(s.x + (s.width - size.x) * 0.5f, s.y + (s.height - size.y) * 0.5f);
                winPos = new Rect(pos, size);
            }
            instance.position = winPos;
            instance.ShowModalUtility();
        }

        private void OnEnable()
        {
            workSetting = new SystemData();
            workSetting.Load();
            serializedObject = new SerializedObject(this);
            //Debug.Log("SettingEdit_Window.Enable");
        }

        private void OnDisable()
        {
            serializedObject.Dispose();
            //Debug.Log("SettingEdit_Window.Disable");
        }

        private void OnGUI()
        {
            serializedObject.Update();

            using (var a = new EditorGUILayout.ScrollViewScope(scrollPos))
            {
                scrollPos = a.scrollPosition;
                var iter = serializedObject.FindProperty("workSetting");
                iter.NextVisible(true);
                do { EditorGUILayout.PropertyField(iter, true); }
                while (iter.NextVisible(false));
            }
            EditorGUILayout.Separator();

            using (var h = new EditorGUILayout.HorizontalScope())
            {
                if (GUILayout.Button("Reset"))
                {
                    workSetting.Load();
                }
                if (GUILayout.Button("Save"))
                {
                    workSetting.Save();
                    winPos = position;
                    Close();
                    ShowWindow();
                    return;
                }
                if (GUILayout.Button("Cancel"))
                {
                    winPos = Rect.zero;
                    Close();
                    return;
                }
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}