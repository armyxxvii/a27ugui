﻿using System;
using UnityEditor;
using UnityEngine;

namespace A27.UGUI
{
    [CustomPropertyDrawer(typeof(ArrayEnumAttribute))]
    public class ArrayEnumAttributeDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            var a = attribute as ArrayEnumAttribute;
            using (var c = new EditorGUI.ChangeCheckScope())
            {
                int newi = EditorGUI.Popup(position, label.text, Array.IndexOf(a.keys, property.stringValue), a.keys);
                if (c.changed) property.stringValue = a.keys[newi];
            }
        }
    }
}