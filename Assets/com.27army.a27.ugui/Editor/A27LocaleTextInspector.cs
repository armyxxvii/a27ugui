﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace A27.UGUI
{
    [CustomEditor(typeof(A27LocaleText))]
    [CanEditMultipleObjects]
    public class A27LocaleTextInspector : UnityEditor.UI.TextEditor
    {
        [MenuItem("GameObject/UI/Text - A27-Locale", priority = 2001)]
        static void New()
        {
            GameObject _parent = Selection.activeGameObject;
            GameObject newVol = new GameObject("New Text-A27-Locale", typeof(A27LocaleText));
            newVol.transform.parent = _parent.transform;
            newVol.GetComponent<A27LocaleText>().text = newVol.name;
        }

        A27LocaleText m_t;
        SerializedProperty p_key;

        protected override void OnEnable()
        {
            m_t = (A27LocaleText)target;
            p_key = serializedObject.FindProperty("key");

            base.OnEnable();
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            using (var c = new EditorGUI.ChangeCheckScope())
            {
                EditorGUILayout.PropertyField(p_key);
                if (c.changed)
                {
                    serializedObject.ApplyModifiedProperties();
                    m_t.gameObject.name = "txt2_" + m_t.key.Key;
                    m_t.Translate();
                    return;
                }
            }
            if (GUILayout.Button("Revert to Text"))
            {
                var id = m_t.GetInstanceID();
                var t = m_t.text;
                var f = m_t.font;
                var fst = m_t.fontStyle;
                var fsz = m_t.fontSize;
                var ls = m_t.lineSpacing;
                var r = m_t.supportRichText;
                var a = m_t.alignment;
                var abg = m_t.alignByGeometry;
                var ho = m_t.horizontalOverflow;
                var vo = m_t.verticalOverflow;
                var bf = m_t.resizeTextForBestFit;
                var c = m_t.color;
                var m = m_t.material;
                var rt = m_t.raycastTarget;
                Text nt = m_t.gameObject.AddComponent<Text>();
                nt.text = t;
                nt.font = f;
                nt.fontStyle = fst;
                nt.fontSize = fsz;
                nt.lineSpacing = ls;
                nt.supportRichText = r;
                nt.alignment = a;
                nt.alignByGeometry = abg;
                nt.horizontalOverflow = ho;
                nt.verticalOverflow = vo;
                nt.resizeTextForBestFit = bf;
                nt.color = c;
                nt.material = m;
                nt.raycastTarget = rt;
                DestroyImmediate(m_t);
                //Debug.Log(id);
                return;
            }
            EditorGUILayout.Separator();

            serializedObject.ApplyModifiedProperties();
            base.OnInspectorGUI();
        }
    }
}