﻿using UnityEngine;
using UnityEditor;

namespace A27.UGUI
{
    [CustomEditor(typeof(A27LocaleImage))]
    [CanEditMultipleObjects]
    public class A27LocaleImageInspector : UnityEditor.UI.ImageEditor
    {
        [MenuItem("GameObject/UI/Image - A27-Locale", priority = 2002)]
        static void New()
        {
            GameObject _parent = Selection.activeGameObject;
            GameObject newVol = new GameObject("New Image-A27-Locale", new[] { typeof(A27LocaleImage) });
            newVol.transform.parent = _parent.transform;
        }

        SerializedProperty p_lss;

        protected override void OnEnable()
        {
            p_lss = serializedObject.FindProperty("localeSpriteSet");

            base.OnEnable();
        }

        public override void OnInspectorGUI()
        {
            if (A27LocaleImage.localeFixed)
            {
                serializedObject.SetIsDifferentCacheDirty();
                A27LocaleImage.localeFixed = false;
            }

            serializedObject.Update();

            EditorGUILayout.Toggle(A27LocaleImage.localeFixed);
            EditorGUILayout.PropertyField(p_lss, true);
            if (GUILayout.Button("Check LocaleSet"))
            {
                foreach (Object t in targets)
                {
                    var li = (A27LocaleImage)t;
                    Undo.RecordObject(li, "Check LocaleSet");
                    li.CheckLocale();
                }
            }
            EditorGUILayout.Separator();

            serializedObject.ApplyModifiedProperties();
            base.OnInspectorGUI();
        }
    }
}