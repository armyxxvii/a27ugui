﻿using UnityEngine;
using UnityEditor;
using System;

namespace A27.UGUI
{
    [CustomPropertyDrawer(typeof(LocaleKey))]
    public class LocaleKeyDrawer : PropertyDrawer
    {
        //const float W = 60;
        const float indentW = 12;

        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            float singleH = EditorGUIUtility.singleLineHeight;
            SerializedProperty p_type = property.FindPropertyRelative("type");
            SerializedProperty p_id = property.FindPropertyRelative("id");
            string[] idList = LocaleLoader.GetMatch("UI" + p_type.stringValue);
            for (int i = 0; i < idList.Length; i++)
            {
                idList[i] = idList[i].Replace("UI" + p_type.stringValue, "");
            }

            float pX = position.x;
            float pW = position.width;

            float labelW_def = EditorGUIUtility.labelWidth;
            EditorGUIUtility.labelWidth = 0;
            int indent_def = EditorGUI.indentLevel;
            EditorGUI.indentLevel = 0;

            //line 1
            //p.width -= W * 3;
            position.height = singleH;
            GUI.Label(position, "UI" + p_type.stringValue + p_id.stringValue);
            //p.x += p.width;
            //p.width = W;
            //using (var c = new EditorGUI.ChangeCheckScope())
            //{
            //    LocaleLoader.Language = (SystemLanguage)EditorGUI.EnumPopup(p, LocaleLoader.Language);
            //    if (c.changed)
            //    {
            //        typeStr = LocaleLoader.Setting.UIType[typeIndex];
            //        p_type.stringValue = typeStr;
            //        idList = LocaleLoader.GetMatch("UI" + typeStr);
            //        idIndex = 0;
            //        p_id.stringValue = " (???)";
            //        return;
            //    }
            //}
            //p.x += p.width;
            //p.width -= 7;
            //if (GUI.Button(p, "Refresh"))
            //{
            //    LocaleLoader.Translate();
            //    idList = LocaleLoader.GetMatch("UI" + typeStr);
            //}
            //p.x += p.width;
            //p.width += 14;
            //if (GUI.Button(p, "RefreshAll"))
            //{
            //    idList = LocaleLoader.GetMatch("UI" + typeStr);
            //    SceneView.RepaintAll();
            //}

            //line 2
            pX += indentW;
            pW -= indentW;
            position.x = pX;
            position.y += singleH;
            position.width = pW / 3;
            EditorGUI.PropertyField(position, p_type, GUIContent.none);

            position.x += position.width;
            position.width *= 2;
            using (var c = new EditorGUI.ChangeCheckScope())
            {
                int idIndex = EditorGUI.Popup(position, "", Array.IndexOf(idList, p_id.stringValue), idList);
                if (c.changed)
                    p_id.stringValue = idList[idIndex];
            }

            //line 3
            string content = LocaleLoader.GetString("UI" + p_type.stringValue + p_id.stringValue);
            position.x = pX;
            position.y += singleH;
            position.width = pW;
            position.height = singleH * 2f;
            if (LocaleLoader.IsMissing(content))
                EditorGUI.HelpBox(position, "找不到編碼 : " + content, MessageType.Error);
            else
                EditorGUI.HelpBox(position, content, MessageType.Info);

            EditorGUIUtility.labelWidth = labelW_def;
            EditorGUI.indentLevel = indent_def;
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            return EditorGUIUtility.singleLineHeight * 4;
        }
    }
}