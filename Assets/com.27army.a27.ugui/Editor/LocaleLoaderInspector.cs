using UnityEngine;
using UnityEditor;

namespace A27.UGUI
{
    [CustomEditor(typeof(LocaleLoader))]
    public class LocaleLoaderInspector : Editor
    {
        private static GUIContent l_setting;
        private static GUIContent l_user;

        private void OnEnable()
        {
            var t = (LocaleLoader)target;
            l_setting = new GUIContent("System Setting");
            l_user = new GUIContent("User Setting");
            t.Invoke("UpdateTempSetting", 0f);
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("keys"), true);
            EditorGUILayout.PropertyField(serializedObject.FindProperty("OnButtonAct"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("OnUserChanged"));
            EditorGUILayout.Separator();

            using (new EditorGUI.DisabledGroupScope(true))
            {
                var p_setting = serializedObject.FindProperty("tempSystemSetting");
                if (p_setting == null)
                    EditorGUILayout.HelpBox("Setting not found...", MessageType.Error);
                else
                    EditorGUILayout.PropertyField(p_setting, l_setting, true);
            }
            GUILayout.TextArea(SystemData.Path);
            EditorGUILayout.Separator();

            using (new EditorGUI.DisabledGroupScope(true))
            {
                var p_user = serializedObject.FindProperty("tempUserSetting");
                if (p_user == null)
                    EditorGUILayout.HelpBox("User setting not found...", MessageType.Error);
                else
                    EditorGUILayout.PropertyField(p_user, l_user, true);
            }
            GUILayout.TextArea(UserData.Path);

            serializedObject.ApplyModifiedProperties();
        }
    }
}