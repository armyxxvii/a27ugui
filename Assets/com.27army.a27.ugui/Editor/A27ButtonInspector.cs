﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

namespace A27.UGUI
{
    [CustomEditor(typeof(A27Button))]
    [CanEditMultipleObjects]
    public class A27ButtonInspector : UnityEditor.UI.ButtonEditor
    {
        [MenuItem("GameObject/UI/Button - A27-Locale", priority = 2031)]
        static void New()
        {
            GameObject _parent = Selection.activeGameObject;
            GameObject newVol = new GameObject("New Button-A27-Locale", new[] { typeof(A27Button) });
            newVol.transform.parent = _parent.transform;
            newVol.AddComponent<A27LocaleImage>();
        }


        bool buttonActFold = true;
        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            base.OnInspectorGUI();
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onSelect"));
            EditorGUILayout.PropertyField(serializedObject.FindProperty("onDeselect"));
            EditorGUILayout.Separator();

            buttonActFold = EditorGUILayout.Foldout(buttonActFold, "ButtonActs");
            if (buttonActFold)
            {
                using (var ind = new EditorGUI.IndentLevelScope())
                {
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("actSubmit"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("actSelect"));
                    EditorGUILayout.PropertyField(serializedObject.FindProperty("actDeselect"));
                }
            }
            EditorGUILayout.Separator();

            EditorGUILayout.PropertyField(serializedObject.FindProperty("m_connectedSelectables"), true);
            var p_cgs = serializedObject.FindProperty("m_connectedGraphics");
            EditorGUILayout.PropertyField(p_cgs, true);
            if (p_cgs.isExpanded)
                EditorGUILayout.PropertyField(serializedObject.FindProperty("m_colorBlock"), true);
            EditorGUILayout.Separator();

            if (GUILayout.Button("Revert to Button"))
            {
                var oldB = (Button)target;
                GameObject o = oldB.gameObject;
                var id = oldB.GetInstanceID();
                var nav = oldB.navigation;
                var tran = oldB.transition;
                var colors = oldB.colors;
                var spri = oldB.spriteState;
                var inter = oldB.interactable;
                var targetG = oldB.targetGraphic;
                var onclick = oldB.onClick;
                DestroyImmediate(oldB);
                Button newB = o.AddComponent<Button>();
                newB.navigation = nav;
                newB.transition = tran;
                newB.colors = colors;
                newB.spriteState = spri;
                newB.interactable = inter;
                newB.targetGraphic = targetG;
                newB.onClick = onclick;
                //Debug.Log("ButtonChanged: " + id.ToString(), o);
                return;
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}