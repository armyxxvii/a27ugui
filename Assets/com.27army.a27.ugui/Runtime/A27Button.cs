﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace A27.UGUI
{
    /// <summary>
    /// 參考 : https://chowdera.com/2022/01/202201262101056357.html
    /// </summary>
    [AddComponentMenu("UI/Button - A27-Locale", 30)]
    public class A27Button : Button
    {
        [SerializeField] private Selectable[] m_connectedSelectables = new Selectable[0];
        [SerializeField] private Graphic[] m_connectedGraphics = new Graphic[0];
        [SerializeField] private ColorBlock m_colorBlock;
        [ArrayEnum(ArrayEnumAttribute.Types.ButtonActType)]
        [SerializeField] private string actSubmit = "Submit", actSelect = "Select", actDeselect = "Cancel";

        void SetColor(Graphic a_graphic, SelectionState a_state)
        {
            Color c = a_state switch
            {
                SelectionState.Highlighted => m_colorBlock.highlightedColor,
                SelectionState.Pressed => m_colorBlock.pressedColor,
                SelectionState.Selected => m_colorBlock.selectedColor,
                SelectionState.Disabled => m_colorBlock.disabledColor,
                _ => m_colorBlock.normalColor,
            };
            a_graphic.CrossFadeColor(c, m_colorBlock.fadeDuration, true, true);
        }

        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            base.DoStateTransition(state, instant);
            foreach (var g in m_connectedGraphics)
                if (g != null) SetColor(g, state);
        }

        public override void OnSubmit(BaseEventData eventData)
        {
            LocaleLoader.Act(actSubmit);
            base.OnSubmit(eventData);
        }
        public override void OnPointerClick(PointerEventData eventData)
        {
            LocaleLoader.Act(actSubmit);
            base.OnPointerClick(eventData);
        }

        public ButtonClickedEvent onSelect;
        public override void OnSelect(BaseEventData eventData)
        {
            LocaleLoader.Act(actSelect);
            onSelect.Invoke();
            base.OnSelect(eventData);

            foreach (var s in m_connectedSelectables)
                s.OnSelect(eventData);
        }
        public override void OnPointerEnter(PointerEventData eventData)
        {
            Select();
        }

        public ButtonClickedEvent onDeselect;
        public override void OnDeselect(BaseEventData eventData)
        {
            LocaleLoader.Act(actDeselect);
            onDeselect.Invoke();
            base.OnDeselect(eventData);

            foreach (var s in m_connectedSelectables)
                s.OnDeselect(eventData);
        }
    }
}