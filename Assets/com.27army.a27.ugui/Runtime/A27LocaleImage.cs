﻿using UnityEngine;
using UnityEngine.UI;
using System;

namespace A27.UGUI
{
    [AddComponentMenu("UI/Image - A27-Locale", 11)]
    public class A27LocaleImage : Image
    {
        [Serializable]
        public class LocaleSpriteSet
        {
            [SerializeField]
            public LocaleSprite[] sprites;
            public Sprite Sprite
            {
                get 
                { 
                    return Array.Find(sprites, x => x.type == UserData.Language).sprite;
                }
            }
        }
        [Serializable]
        public struct LocaleSprite
        {
            [ArrayEnum(ArrayEnumAttribute.Types.LanguageType)]
            public string type;
            public Sprite sprite;
        }

        [SerializeField] private LocaleSpriteSet localeSpriteSet;

        protected override void Start()
        {
            if (!Application.isPlaying) return;
            Translate();
        }

        public void Translate()
        {
            var newSprite = localeSpriteSet.Sprite;
            if (newSprite != null)
                sprite = newSprite;
        }

        public static bool localeFixed = false;
        public void CheckLocale()
        {
            string[] t = SystemData.Global.LanguageType;
            LocaleSprite[] newLss = new LocaleSprite[t.Length];
            for (int i = 0; i < t.Length; i++)
            {
                int n = Array.FindIndex(localeSpriteSet.sprites, x => x.type == t[i]);
                if (n > 0)
                {
                    newLss[i] = localeSpriteSet.sprites[n];
                    if (newLss[i].sprite == null)
                        newLss[i].sprite = sprite;
                }
                else
                    newLss[i] = new LocaleSprite()
                    {
                        type = t[i],
                        sprite = sprite
                    };
            }
            localeSpriteSet.sprites = newLss;
            localeFixed = true;
        }
    }
}