﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

namespace A27.UGUI
{
    public partial class LocaleLoader : MonoBehaviour
    {
        #region Component
        private static LocaleLoader Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = FindObjectOfType<LocaleLoader>();
                    if (instance == null)
                    {
                        Canvas c = FindObjectOfType<Canvas>();
                        if (c == null)
                            instance = new GameObject("Canvas", typeof(Canvas)).AddComponent<LocaleLoader>();
                        else
                            instance = c.gameObject.AddComponent<LocaleLoader>();
                    }
                }
                return instance;
            }
        }
        private static LocaleLoader instance;

        //只是用來在 Inspector 加個測試工具
        [SerializeField] private LocaleKey[] keys;
        [SerializeField] private SystemData tempSystemSetting;
        [SerializeField] private UserData tempUserSetting;
        private void Awake()
        {
            if (instance != null)
            {
                gameObject.SetActive(false);
                return;
            }
            instance = this;
            UpdateTempSetting();
            table = Table;
            RefreshCanvas(false);

            OnButtonAct.AddListener(key =>
            {
                //Debug.Log("ButtonAct：" + key);
            });
            OnUserChanged.AddListener(() =>
            {
                //Debug.Log("OnUserChanged");
                RefreshCanvas();
            });
        }
        private void UpdateTempSetting()
        {
            //Debug.Log("UpdateTempSetting");
            tempSystemSetting.Load();
            tempUserSetting.Load();
        }
        private void RefreshCanvas(bool a_loadTable = true)
        {
            if (a_loadTable) LoadTable();

            //Debug.Log("RefreshCanvas");
            var Ts = transform.GetComponentsInChildren<A27LocaleText>(true);
            foreach (var T in Ts) T.Translate();
            var Is = transform.GetComponentsInChildren<A27LocaleImage>(true);
            foreach (var I in Is) I.Translate();
        }
        private void RefreshImages()
        {
            var Is = transform.GetComponentsInChildren<A27LocaleImage>(true);
            foreach (var I in Is) I.CheckLocale();
        }
        #endregion

        private static Dictionary<string, string> table;
        private static Dictionary<string, string> Table
        {
            get
            {
                if (table == null)
                {
                    table = new Dictionary<string, string>();
                    LoadTable();
                }
                return table;
            }
        }
        private static void GetTableFromFile(string data)
        {
            int beginindex = -1;
            for (int i = 0; i < data.Length; ++i)
            {
                if (data[i] == SystemData.Global.BeginMark)
                {
                    beginindex = i;
                }
                else if (data[i] == SystemData.Global.EndMark && beginindex >= 0)
                {
                    string s = data.Substring(beginindex + 1, i - beginindex - 1);
                    string[] p = s.Split(new string[] { SystemData.Global.SplitMark }, StringSplitOptions.None);

                    if (table.ContainsKey(p[0]))
                    {
                        Debug.LogWarning("String Table has Repeated Key: " + p[0]);
                        continue;
                    }
                    else
                    {
                        table.Add(p[0], p[1]);
                    }
                    beginindex = -1;
                }
            }
        }
        private static void LoadTable()
        {
            string folder = Application.streamingAssetsPath + "/A27/UGUI/" + UserData.Language.ToString() + "/";
            string[] fileList = Directory.GetFiles(folder, "*.txt");
            if (fileList.Length == 0)
            {
                Debug.LogWarning("Translate files not found");
                return;
            }

            Table.Clear();
            for (int i = 0; i < fileList.Length; ++i)
            {
                using (UnityWebRequest request = UnityWebRequest.Get(fileList[i]))
                {
                    UnityWebRequestAsyncOperation rao = request.SendWebRequest();
                    while (!rao.isDone) { }

                    if (request.result == UnityWebRequest.Result.Success)
                        GetTableFromFile(request.downloadHandler.text);
                    else
                        Debug.LogError(request.error);
                }
            }
        }


        public static void AddLoader()
        {
            //Debug.Log(Instance);
        }
        public static void Translate()
        {
            Instance.RefreshCanvas();
        }
        public static void UpdateAllImages()
        {
            Instance.RefreshImages();
        }
        public static string GetString(string a_key)
        {
            return Table.ContainsKey(a_key) ? Table[a_key] : SystemData.Global.MissingMark + a_key;
        }
        public static string[] GetMatch(string a_keyWord)
        {
            List<string> temp = new List<string>();
            switch (a_keyWord)
            {
                case "":
                case "UI":
                    return temp.ToArray();

                default:
                    foreach (var k in Table.Keys)
                    {
                        if (k.Contains(a_keyWord))
                            temp.Add(k);
                    }
                    return temp.ToArray();
            }

        }
        public static bool IsMissing(string a_key)
        {
            return a_key.Contains(SystemData.Global.MissingMark);
        }

        #region ActEvent
        public static void Act(string key)
        {
            if (instance.OnButtonAct != null) instance.OnButtonAct.Invoke(key);
            else Debug.LogWarning("event ButtonAct is null ！");
        }
        public UnityEvent<string> OnButtonAct;

        public static void UserSettingChanged()
        {
            if (Instance.OnUserChanged != null)
                instance.OnUserChanged.Invoke();
        }
        public UnityEvent OnUserChanged;
        #endregion
    }
}