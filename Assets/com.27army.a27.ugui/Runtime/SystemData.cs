﻿using System;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;

namespace A27.UGUI
{
    [Serializable]
    public class SystemData
    {
        public string SplitMark = ":=";
        public string MissingMark = "!@-";
        public char BeginMark = '┤', EndMark = '├';
        public string[] UIType = {
            "Panel",
            "Label",
            "Button",
            "MenuItem",
            "Status",
            "Tip",
            "Manual",
            "MsgBox",
            "System",
            "Speech"
        };
        public string[] ButtonActType =
        {
            "Select",
            "Submit",
            "Cancel",
            "Achieve",
            "Flip"
        };
        public string[] LanguageType { get => Array.ConvertAll(languageType, x => x.ToString()); }
        [SerializeField]
        private SystemLanguage[] languageType =
        {
            SystemLanguage.English,
            SystemLanguage.Japanese,
            SystemLanguage.ChineseSimplified,
            SystemLanguage.ChineseTraditional
        };
        [ArrayEnum(ArrayEnumAttribute.Types.LanguageType)]
        public string DefaultLanguage = SystemLanguage.English.ToString();


        public void Save()
        {
            string data = JsonUtility.ToJson(this);
            File.WriteAllText(Path, data);
            //Debug.Log("Save System setting!");

            Global.Load();
        }

        public void Load()
        {
            using (UnityWebRequest request = UnityWebRequest.Get(Path))
            {
                UnityWebRequestAsyncOperation rao = request.SendWebRequest();

                string log = "Load System setting!\n<color=grey>";
                int wait = 0;
                while (!rao.isDone)
                {
                    wait++;
                    log += wait + "：" + rao.progress + "\n";
                }
                log += "</color>download finish：" + wait + "\n\n";

                if (request.result == UnityWebRequest.Result.Success)
                {
                    string data = request.downloadHandler.text;
                    SystemData work = JsonUtility.FromJson<SystemData>(data);

                    SplitMark = work.SplitMark;
                    MissingMark = work.MissingMark;
                    BeginMark = work.BeginMark;
                    UIType = work.UIType;
                    ButtonActType = work.ButtonActType;
                    languageType = work.languageType;
                    DefaultLanguage = work.DefaultLanguage;

                    //Debug.Log(log);
                }
                else Debug.LogError(request.error);
            }
        }


        public static string Path
        {
            get
            {
#if UNITY_EDITOR
                string path = Application.streamingAssetsPath + "/A27/UGUI";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                path += "/System.json";
                if (!File.Exists(path))
                {
                    StreamWriter sw = File.CreateText(path);
                    sw.Write(JsonUtility.ToJson(new SystemData()));
                    sw.Close();
                    UnityEditor.AssetDatabase.Refresh();
                }
                return path;
#else
                return Application.streamingAssetsPath + "/A27/UGUI/System.json";
#endif
            }
        }

        public static SystemData Global
        {
            get
            {
                if (global == null)
                {
                    global = new SystemData();
                    global.Load();
                }
                return global;
            }
        }
        private static SystemData global;
    }
}