﻿using System;
using System.IO;
using UnityEngine;

namespace A27.UGUI
{
    [Serializable]
    public class UserData
    {
        [ArrayEnum(ArrayEnumAttribute.Types.LanguageType)]
        public string language = "English";

        public void Save()
        {
            string data = JsonUtility.ToJson(this);
            File.WriteAllText(Path, data);

            //Debug.Log("Save user setting!");
            if (!Equals(global))
                Global.Load();

            if (Application.isPlaying)
                LocaleLoader.UserSettingChanged();
            else
                LocaleLoader.Translate();
        }

        public void Load()
        {
            string data = File.ReadAllText(Path);
            UserData work = JsonUtility.FromJson<UserData>(data);

            language = work.language;

            //Debug.Log("Load user setting!");
        }


        public static string Path
        {
            get
            {
#if UNITY_EDITOR
                string path = Application.persistentDataPath + "/A27/UGUI";
                if (!Directory.Exists(path))
                    Directory.CreateDirectory(path);

                path += "/User.json";
                if (!File.Exists(path))
                {
                    StreamWriter sw = File.CreateText(path);
                    sw.Write(JsonUtility.ToJson(new UserData()));
                    sw.Close();
                    UnityEditor.AssetDatabase.Refresh();
                }
                return path;
#else
                return Application.persistentDataPath + "/A27/UGUI/User.json";
#endif
            }
        }

        public static UserData Global
        {
            get
            {
                if (global == null)
                {
                    global = new UserData();
                    global.Load();
                }
                return global;
            }
        }
        private static UserData global;
        public static string Language
        {
            get { return Global.language; }
        }
    }
}