﻿using UnityEngine;

namespace A27.UGUI
{
    public class ArrayEnumAttribute : PropertyAttribute
    {
        public enum Types
        {
            UIType,
            LanguageType,
            ButtonActType,
        }
        public string[] keys;
        public ArrayEnumAttribute(Types type)
        {
            switch (type)
            {
                case Types.UIType:
                    keys = SystemData.Global.UIType;
                    return;
                case Types.LanguageType:
                    keys = SystemData.Global.LanguageType;
                    return;
                case Types.ButtonActType:
                    keys = SystemData.Global.ButtonActType;
                    return;
            }
        }
    }
}