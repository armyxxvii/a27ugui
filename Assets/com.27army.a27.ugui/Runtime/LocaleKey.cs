﻿namespace A27.UGUI
{
    [System.Serializable]
    public class LocaleKey
    {
        [ArrayEnum(ArrayEnumAttribute.Types.UIType)]
        public string type;
        public string id;

        public string Key { get { return "UI" + type + id; } }
    }
}
