﻿using UnityEngine;
using UnityEngine.UI;

namespace A27.UGUI
{
    [AddComponentMenu("UI/Text - A27-Locale", 10)]
    public partial class A27LocaleText : Text
    {
        public LocaleKey key;

        protected override void Start()
        {
            if (!Application.isPlaying) return;
            Translate();
        }

        public void Translate()
        {
            string content = LocaleLoader.GetString(key.Key);
            text = content;
        }
    }
}
